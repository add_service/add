/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Package main implements a client for Greeter service.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "protos"
)


var (
	addr = flag.String("addr", "localhost:50051", "the address to connect to")
)

// func main() {
// 	flag.Parse()
// 	// Set up a connection to the server.
// 	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
// 	if err != nil {
// 		log.Fatalf("did not connect: %v", err)
// 	}
// 	defer conn.Close()
// 	c := pb.NewGreeterClient(conn)
  
// 	var (
// 		a int32
// 		b int32
// 	)

// 	fmt.Println("Enter first number:")
// 	fmt.Scan(&a)


// 	fmt.Println("Enter second number:")
// 	fmt.Scan(&b)


// 	// Contact the server and print out its response.
// 	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
// 	defer cancel()
// 	_, err = c.Add(ctx, &pb.Numbers{
// 		A: a,
// 		B: b,
// 	})
// 	if err != nil {
// 		log.Fatalf("could not greet: %v", err)
// 	}
// }



// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(ctx context.Context, in *pb.Numbers) (*pb.Result, error) {
	return &pb.Result{
		Result: in.A + in.B,
	}, nil
}

// Add implements helloworld.GreeterServer
func (s *server) Add(ctx context.Context, in *pb.Numbers) (*pb.Result, error) {
	return &pb.Result{
		Result: in.A + in.B,
	}, nil
}

// client main function
func main() {
	flag.Parse()
	// Set up a connection to the server.
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn)
  
	var (
		a int32
		b int32
	)

	fmt.Println("Enter first number:")
	fmt.Scan(&a)

	fmt.Println("Enter second number:")
	fmt.Scan(&b)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Call Add method to get the sum of two numbers
	addResult, err := c.Add(ctx, &pb.Numbers{
		A: a,
		B: b,
	})
	if err != nil {
		log.Fatalf("could not add: %v", err)
	}

	// Print out the result of the addition
	fmt.Printf("Result of %d + %d = %d\n", a, b, addResult.Result)
}
